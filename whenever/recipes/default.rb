#
# Cookbook Name:: whenever
# Recipe:: default
#
first_hostname = node[:opsworks][:layers]['rails-app'][:instances].keys.sort.first

if first_hostname.present?
  log 'message' do
    message 'The first hostname is '+first_hostname+'. This hostname is '+node[:opsworks][:instance][:hostname]
    level :info
  end
else
  log 'message' do
    message 'The first hostname is not set. This hostname is '+node[:opsworks][:instance][:hostname]
    level :warn
  end
end

if first_hostname.present? and first_hostname == node[:opsworks][:instance][:hostname]
  log 'message' do
    message 'Adding crontab'
    level :info
  end  
  node[:deploy].each do |application, deploy|
    deploy = node[:deploy][application]

    ENV['AWS_ACCESS_KEY_ID'] = deploy[:environment_variables][:AWS_ACCESS_KEY_ID]
    ENV['AWS_SECRET_ACCESS_KEY'] = deploy[:environment_variables][:AWS_SECRET_ACCESS_KEY]
    ENV['AWS_REGION'] = deploy[:environment_variables][:AWS_REGION]
    ENV['FOG_DIRECTORY'] = deploy[:environment_variables][:FOG_DIRECTORY]
    ENV['S3_BUCKET_NAME'] = deploy[:environment_variables][:FOG_DIRECTORY]

    log 'message' do
      message 'AWS_ACCESS_KEY_ID is $AWS_ACCESS_KEY_ID.'
      level :info
    end

    bash "update-crontab-#{application}" do
      layers = node[:opsworks][:instance][:layers]
      cwd "#{deploy[:deploy_to]}/current"
      user 'deploy'
      code "bundle exec whenever --set environment=#{deploy[:rails_env]} --update-crontab #{application} --roles #{layers.join(',')}"
      only_if "cd #{deploy[:deploy_to]}/current && bundle show whenever"
    end

    bash "update-delayed-job-#{application}" do
      cwd "#{deploy[:deploy_to]}/current"
      user 'deploy'
      code "RAILS_ENV=production bin/delayed_job restart"
    end
  end

# else
#   log 'message' do
#     message 'Removing crontab'
#     level :info
#   end   
#   node[:deploy].each do |application, deploy|
#     bash "update-crontab-#{application}" do  
#       user 'deploy'
#       code 'crontab -u deploy -r'
#       only_if 'crontab -u deploy -l'
#     end
#   end
end