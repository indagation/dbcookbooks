name             'whenever'
maintainer       'indagation'
maintainer_email 'indagation@gmail.com'
license          'All rights reserved'
description      'Configures whenever'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
