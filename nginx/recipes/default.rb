#
# Cookbook Name:: nginx
# Recipe:: default
# Author:: AJ Christensen <aj@junglist.gen.nz>
#
# Copyright 2008, OpsCode, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


package "openssl-devel" do
  retries 3
  retry_delay 5
end

package "gcc" do
  retries 3
  retry_delay 5
end

package "pcre-devel" do
  retries 3
  retry_delay 5
end

package "nginx" do
  retries 3
  retry_delay 5
  not_if { File.exist?('/usr/sbin/nginx' ) && File.exist?('/etc/rc.d/init.d/nginx')}
end

bash 'install_nginx' do
  user 'root'
  group 'root'
  cwd '/tmp'
  code <<-EOH
    wget https://nginx.org/download/nginx-1.21.1.tar.gz
    tar zxf nginx-1.21.1.tar.gz
    cd nginx-1.21.1
    ./configure --with-http_ssl_module --with-stream --conf-path=/etc/nginx/nginx.conf --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --pid-path=/var/run/nginx.pid --with-http_stub_status_module --with-http_gzip_static_module
    make
    make install
  EOH
end

directory node[:nginx][:dir] do
  owner 'root'
  group 'root'
  mode '0755'
end

directory node[:nginx][:log_dir] do
  mode 0755
  owner node[:nginx][:user]
  action :create
end

%w{sites-available sites-enabled conf.d}.each do |dir|
  directory File.join(node[:nginx][:dir], dir) do
    owner 'root'
    group 'root'
    mode '0755'
  end
end

%w{nxensite nxdissite}.each do |nxscript|
  template "/usr/sbin/#{nxscript}" do
    source "#{nxscript}.erb"
    mode 0755
    owner "root"
    group "root"
  end
end

template "nginx.conf" do
  path "#{node[:nginx][:dir]}/nginx.conf"
  source "nginx.conf.erb"
  owner "root"
  group "root"
  mode 0644
end

template "#{node[:nginx][:dir]}/sites-available/default" do
  source "default-site.erb"
  owner "root"
  group "root"
  mode 0644
end

include_recipe "nginx::service"

service "nginx" do
  action [ :enable, :start ]
end
