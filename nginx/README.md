SSL Cipher Configuration derived from:
https://docs.aws.amazon.com/elasticloadbalancing/latest/network/create-tls-listener.html

General Configuration partially derived from:
https://ssl-config.mozilla.org/

Test with:
https://www.ssllabs.com/ssltest/analyze.html?d=onlinedrivers.education
https://www.digicert.com/help/

Also setup http2
https://www.digitalocean.com/community/tutorials/how-to-set-up-nginx-with-http-2-support-on-ubuntu-18-04#:~:text=Nginx%20is%20a%20fast%20and%20reliable%20open%2Dsource%20web%20server.&text=HTTP%2F2%20is%20a%20new,in%20almost%20two%20decades%3A%20HTTP1.


# generated 2020-12-29, Mozilla Guideline v5.6, nginx 1.17.7, OpenSSL 1.1.1d, intermediate configuration
# https://ssl-config.mozilla.org/#server=nginx&version=1.17.7&config=intermediate&openssl=1.1.1d&guideline=5.6
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    ssl_certificate /path/to/signed_cert_plus_intermediates;
    ssl_certificate_key /path/to/private_key;
    ssl_session_timeout 1d;
    ssl_session_cache shared:MozSSL:10m;  # about 40000 sessions
    ssl_session_tickets off;

    # curl https://ssl-config.mozilla.org/ffdhe2048.txt > /path/to/dhparam
    ssl_dhparam /path/to/dhparam;

    # intermediate configuration
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_prefer_server_ciphers off;

    # HSTS (ngx_http_headers_module is required) (63072000 seconds)
    add_header Strict-Transport-Security "max-age=63072000" always;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;

    # verify chain of trust of OCSP response using Root CA and Intermediate certs
    ssl_trusted_certificate /path/to/root_CA_cert_plus_intermediates;

    # replace with the IP address of your resolver
    resolver 127.0.0.1;
}

If there are issues on deployment, then reload old nginx.conf.erb below:
user <%= node[:nginx][:user] %>;
worker_processes  <%= node[:nginx][:worker_processes] %>;

error_log  <%= node[:nginx][:log_dir] %>/error.log;
pid        <%= node[:nginx][:pid_file] %>;

events {
  worker_connections  <%= node[:nginx][:worker_connections] %>;
}

http {
  include       <%= node[:nginx][:dir] %>/mime.types;
  default_type  application/octet-stream;

  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

  <% node[:nginx][:log_format].each do |name, format| %>
  log_format <%= name %> <%= format %>;
  <% end %>

  access_log  <%= node[:nginx][:log_dir] %>/access.log;

  sendfile on;
  tcp_nopush on;
  tcp_nodelay on;

  <% if node[:nginx][:keepalive] == "on" %>
  keepalive_timeout  <%= node[:nginx][:keepalive_timeout] %>;
  <% end %>

  gzip  <%= node[:nginx][:gzip] %>;
  gzip_static  <%= node[:nginx][:gzip_static] %>;
  <% if node[:nginx][:gzip] == "on" %>
  gzip_http_version <%= node[:nginx][:gzip_http_version] %>;
  gzip_comp_level <%= node[:nginx][:gzip_comp_level] %>;
  gzip_proxied <%= node[:nginx][:gzip_proxied] %>;
  gzip_types <%= node[:nginx][:gzip_types].join(' ') %>;
  gzip_vary <%= node[:nginx][:gzip_vary] %>;
  gzip_disable "<%= node[:nginx][:gzip_disable] %>";
  <% end %>

  client_max_body_size <%= node[:nginx][:client_max_body_size] %>;

  server_names_hash_bucket_size 128;

  include <%= node[:nginx][:dir] %>/conf.d/*.conf;
  include <%= node[:nginx][:dir] %>/sites-enabled/*;
}