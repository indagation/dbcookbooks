#
# Cookbook Name:: precompile
# Recipe:: default
#

node[:deploy].each do |application, deploy|
  rails_env = new_resource.environment["RAILS_ENV"]
  Chef::Log.info("Precompiling assets for #{rails_env}...")
    
  current_release = release_path
    
  execute "rake assets:precompile" do
    cwd current_release
    command "bundle exec rake assets:precompile"
    environment "RAILS_ENV" => rails_env
  end  
end

  # rails_env = deploy[:rails_env]
  # current_path = deploy[:current_path]

  # Chef::Log.info("Precompiling Rails assets with environment #{rails_env}")

  # execute 'rake assets:precompile' do
  #   cwd current_path
  #   user 'deploy'
  #   command 'bundle exec rake assets:precompile'
  #   environment 'RAILS_ENV' => rails_env
  # end