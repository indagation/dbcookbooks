name             'precompile'
maintainer       'indagation'
maintainer_email 'indagation@gmail.com'
license          'All rights reserved'
description      'Options for deployment'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
